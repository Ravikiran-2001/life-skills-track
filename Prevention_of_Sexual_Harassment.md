# Prevention of Sexual Harassment
## What kinds of behaviour cause sexual harassment?
Sexual harassment is a deeply concerning issue that can arise from inappropriate behaviors. Unwanted advances, comments, or jokes that make someone uncomfortable, coupled with unwarranted physical contact or pressure, contribute to a hostile environment. Displaying explicit material, engaging in cyber harassment, or making inappropriate comments about appearance are all examples of behaviors that can perpetuate this problem. It's crucial to recognize and address these actions in various settings, whether at work, in education, or online. Creating a culture of respect, consent, and accountability is essential for preventing and combating sexual harassment. Remember, the responsibility lies with the perpetrators, and everyone deserves a safe and respectful environment.

## What would you do in case you face or witness any incident or repeated incidents of such behaviour?
If I were ever to come across instances of inappropriate behavior or witness someone facing harassment, my immediate response would be to take it seriously. First and foremost, I'd ensure the person affected feels supported by offering empathy and encouraging them to report the incidents. Documenting the details would be crucial for any subsequent actions, noting dates, times, and locations.

Depending on the situation, I would then report the behavior through the appropriate channels within the organization or community, adhering to established procedures. Seeking guidance from supervisors or human resources would be a natural step, ensuring that I follow the correct protocol.

Maintaining confidentiality would be a priority, respecting the privacy of those involved. Additionally, I'd encourage others to speak up if they've witnessed or experienced similar incidents, fostering a culture of openness and accountability.

Being proactive against harassment isn't just a responsibility; it's about creating an environment where everyone feels safe and respected. I firmly believe that standing against such behaviors and promoting a culture of understanding is crucial for the well-being of any community or workplace.
