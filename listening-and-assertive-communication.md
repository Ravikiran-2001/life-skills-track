## Question 1
### What are the steps/strategies to do Active Listening?

- Focus on the Speaker and Topic.
- Avoid Interruption.
- Use Door Openers
- Take Note
- Paraphrase to Confirm Understanding
- Express Interest
- Practice Empathy

## Question 2
### According to Fisher's model, what are the key points of Reflective Listening?
- **Confirm Understanding:** Reflective listening involves confirming the speaker's idea to ensure correct understanding.


- **Rooted in Empathy:** Emerged from Carl Rogers' therapy, emphasizing genuine understanding and empathy.


- **Daily Life Application:** Useful in personal relationships, particularly in helping others open up about their feelings.

## Question 3
### What are the obstacles in your listening process?
- Lack of Empathy
- Distractions
- Prejudgment
- Selective Listening

## Question 4
### What can you do to improve your listening?
- Being Kind and Understand Others
- Cut Out Distractions
- Avoid Making Quick Judgments
- Showing that I am Interested
## Question 5
### When do you switch to Passive communication style in your day to day life?
I usually talk in a calm way to keep things friendly and avoid fights. Even though I like being open, sometimes I choose not to say things too strongly because keeping good relationships is more important to me.
## Question 6
### When do you switch into Aggressive communication styles in your day to day life?
In my daily life, when I'm really busy or not feeling great, I sometimes talk more forcefully. In those moments, I might share my thoughts strongly or react more intensely than usual.
## Question 7
### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
In my everyday life, when I'm annoyed or upset, I tend to use indirect ways of communicating, like being sarcastic, talking behind someone's back, making teasing remarks, or staying silent. This happens when I don't feel like or can't express my feelings directly.
## Question 8
### How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? (Watch the videos first before answering this question.)
- Express my thoughts and opinions clearly without being aggressive.
- Maintain good eye contact to show confidence and sincerity.
- Practice active listening to understand others' perspectives.
- Set boundaries and stand firm on my values.
- Choose my words carefully, aiming for directness and respect.
