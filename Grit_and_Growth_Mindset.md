## Question 1
### Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.

In a video, Angela Duckworth emphasizes the significance of passion and perseverance in achieving success in both academics and life. She contends that success is not solely determined by intelligence or appearance but is rather influenced by the ability to persist and view life as a marathon, not a sprint. Duckworth's research indicates that students with higher levels of grit, those who demonstrate resilience and a refusal to give up easily, are more likely to successfully complete their education.
## Question 2
### Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.
In a video, Professor Carol Dweck underscores the significance of fostering a growth mindset, wherein individuals believe that intelligence can be enhanced through effort. This mindset encourages a proactive approach to challenges, learning from setbacks, and a continuous pursuit of improvement. Dweck juxtaposes this with a fixed mindset, characterized by the belief that abilities are rigid and unchangeable. Embracing a growth mindset, as outlined by Dweck, empowers individuals to transcend self-imposed limitations and promotes a mindset centered on continual learning and progress.
## Question 3
### What is the Internal Locus of Control? What is the key point in the video?
The concept of Internal Locus of Control pertains to the belief in having control over one's life and outcomes. The video highlights a study conducted at Columbia University, revealing that fifth graders attributing success to hard work tended to concentrate on simpler tasks and showed less persistence with challenging ones. Conversely, those crediting success to intelligence displayed higher motivation and persistence. The central message of the video is to offer guidance on embracing an internal locus of control as a means to sustain motivation.
## Question 4
### What are the key points mentioned by speaker to build growth mindset (explanation not needed).
- Have Confidence in Your Abilities
- Challenge Your Assumptions
- Create a Blueprint for Aspirations
- Extract Lessons from Setbacks
- Acknowledge and Learn from Challenges
## Question 5
### What are your ideas to take action and build Growth Mindset?
I believe in myself that I can do and work on feedback received from others, learning from my failures as well.





