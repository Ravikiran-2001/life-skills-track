## Question 1
### What are the activities you do that make you relax - Calm quadrant?
- Walks: Enjoy a leisurely stroll in a park or natural surroundings to promote relaxation.
- Deep Breathing: Practice slow, deep breaths to soothe the mind.
- Music: Immerse yourself in calming tunes for a tranquilizing effect.
- Hot Baths/Showers: Indulge in a warm bath to release tension and promote relaxation.
- Journaling: Engage in writing down your thoughts as a method to clear and declutter the mind.
- Disconnect: Schedule breaks from work to prioritize mental peace and rejuvenation.

## Question 2
### When do you find getting into the Stress quadrant?
- Acquiring new skills.
- Engaging in vigorous exercise routines.
- Confronting challenging tasks or meeting high expectations.
- Managing tight deadlines or adapting to unexpected changes.
- Navigating conflicts or alleviating tension in various situations.

## Question 3
### How do you understand if you are in the Excitement quadrant?
During times when:
- I'm brimming with energy.
- My words and actions exude enthusiasm.
- I find myself smiling and laughing more frequently.
- I'm immersed in focus and engagement.
- My attitude remains consistently positive.
- I'm receptive to new ideas and opportunities.
## Question 4
### Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.
- Quality sleep is essential for the well-being of both the body and the brain.
- Inadequate sleep can negatively impact health, elevate heart rates, and contribute to accidents.
- A compromised sleep pattern can weaken the immune system, making the body more susceptible to infections.
- Proper sleep is crucial for memory, learning, and overall mental and physical health.
- A study revealed disrupted gene activity with as little as four hours of sleep.
- Consistent and restful sleep, along with maintaining a cool bedroom environment, enhances the quality of sleep.

## Question 5
### What are some ideas that you can implement to sleep better?
To improve my sleep, I can consider:
- Establishing a consistent bedtime and wake-up routine.
- Creating a cool, dark, and quiet sleep environment in my bedroom.
- Avoiding screens before bedtime to promote better sleep quality.
- Steering clear of heavy meals and excessive drinks close to bedtime.
## Question 6
### Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.
- Minimal exercise enhances mood, attention, and memory, providing immediate benefits.
- Regular physical activity yields long-lasting advantages and serves as a protective measure against neurodegenerative diseases.
- Strive for 30 minutes of daily exercise, incorporating aerobic activities.
- Even one minute of exercise can contribute positively to brain health.
## Question 7
### What are some steps you can take to exercise more?
- Strive for a minimum of 30 minutes of daily exercise, incorporating aerobic activities into my routine.
- Begin with small and manageable exercise sessions to build a consistent habit.
- Discover activities that bring enjoyment to make exercise a more pleasurable experience.
- Establish realistic goals to maintain motivation and a sense of achievement.
- Transform exercise into a social activity by engaging with friends or family during workouts.



















