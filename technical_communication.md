# Object Oriented Programming 
Object-Oriented Programming or OOPs refers to languages that use objects in programming. Object-oriented programming aims to implement real-world entities like inheritance, hiding, polymorphism, etc in programming. The main aim of OOP is to bind together the data and the functions that operate on them so that no other part of the code can access this data except that function.

## Pillars of OOPs
- Abstraction
- Encapsulation
- Inheritance
- Polymorphism

## 1. Abstraction

Abstraction is used to hide background details or any unnecessary implementation about the data so that users only see the required information. It is one of the most important and essential features of object-oriented programming.
```java // Abstract class
abstract class Animal {
    // Abstract method (no implementation)
    abstract void makeSound();
}

// Concrete subclass implementing the abstract class
class Dog extends Animal {
    // Implementation of the abstract method
    @Override
    void makeSound() {
        System.out.println("Woof! Woof!");
    }
}

// Concrete subclass implementing the abstract class
class Cat extends Animal {
    // Implementation of the abstract method
    @Override
    void makeSound() {
        System.out.println("Meow!");
    }
}

public class Main {
    public static void main(String[] args) {
        // Creating objects of concrete classes
        Dog myDog = new Dog();
        Cat myCat = new Cat();

        // Invoking the makeSound method (polymorphism)
        myDog.makeSound();
        myCat.makeSound();
    }
}
```
## 2. Encapsulation
It is defined as the wrapping up of data under a single unit. It is the mechanism that binds together the code and the data it manipulates. Another way to think about encapsulation is that it is a protective shield that prevents the data from being accessed by the code outside this shield. 
```java
public class BankAccount {
    private String accountHolder;
    private double balance;

    // Constructor
    public BankAccount(String accountHolder, double initialBalance) {
        this.accountHolder = accountHolder;
        this.balance = initialBalance;
    }

    // Getter for accountHolder
    public String getAccountHolder() {
        return accountHolder;
    }

    // Getter for balance
    public double getBalance() {
        return balance;
    }

    // Method to deposit money
    public void deposit(double amount) {
        if (amount > 0) {
            balance += amount;
            System.out.println("Deposit successful. New balance: " + balance);
        } else {
            System.out.println("Invalid deposit amount.");
        }
    }

    // Method to withdraw money
    public void withdraw(double amount) {
        if (amount > 0 && amount <= balance) {
            balance -= amount;
            System.out.println("Withdrawal successful. New balance: " + balance);
        } else {
            System.out.println("Invalid withdrawal amount or insufficient funds.");
        }
    }

    public static void main(String[] args) {
        // Creating a BankAccount object
        BankAccount account = new BankAccount("John Doe", 1000.0);

        // Accessing and displaying account information
        System.out.println("Account Holder: " + account.getAccountHolder());
        System.out.println("Initial Balance: " + account.getBalance());

        // Making a deposit
        account.deposit(500.0);

        // Making a withdrawal
        account.withdraw(200.0);
    }
}
```
## 3. Inheritance 
Inheritance is an important pillar of OOP (Object Oriented Programming). It is the mechanism in Java by which one class is allowed to inherit the features (fields and methods) of another class. We are achieving inheritance by using extends keyword. Inheritance is also known as “is-a” relationship.
```java
// Parent class (superclass)
class Animal {
    public void eat() {
        System.out.println("Animal is eating.");
    }
}

// Child class (subclass) inheriting from Animal
class Dog extends Animal {
    public void bark() {
        System.out.println("Dog is barking.");
    }
}

public class Main {
    public static void main(String[] args) {
        // Create an object of the Dog class
        Dog myDog = new Dog();

        // Call methods from the superclass (Animal)
        myDog.eat();

        // Call methods from the subclass (Dog)
        myDog.bark();
    }
}
```
### Types of Inheritance
**1.Single Inheritance:**
- A class can inherit from only one superclass. In other words, a subclass has a single parent class.
> Example: **class Subclass extends Superclass**

**2.Multiple Inheritance:**
- A class can inherit from more than one superclass. It allows a subclass to inherit attributes and behaviors from multiple parent classes.
> Example: **class Subclass extends Superclass1, Superclass2**

**3.Multilevel Inheritance:**
- A class can act as a superclass for another class and as a subclass to yet another class, creating a chain of inheritance.
> Example: **class Grandparent, class Parent extends Grandparent, class Child extends Parent**

**4.Hierarchical Inheritance:**
- Multiple classes inherit from a single superclass. It forms a hierarchy where multiple subclasses share a common parent class.
> Example: **class Parent, class Child1 extends Parent, class Child2 extends Parent**

**5.Hybrid Inheritance:**
- A combination of two or more types of inheritance within a single program. It can involve any combination of the above types.
> Example: **Combining aspects of single, multiple, multilevel, or hierarchical inheritance in a program.**
## 4. Polymorphism
Polymorphism, in the context of object-oriented programming (OOP), refers to the ability of a single function, method, or operator to operate on different types of data or objects. It allows a single interface to represent various implementations, and the appropriate implementation is selected based on the context or the actual type of the object at runtime.
### Types of Polymorphism
**1.Compile-time Polymorphism (Method Overloading):**
- Multiple methods with the same name exist in the same class.
- The compiler determines which method to call based on the number and types of parameters.
```java
class MathOperations {
    int add(int a, int b) {
        return a + b;
    }

    double add(double a, double b) {
        return a + b;
    }
}
```
**2.Runtime Polymorphism (Method Overriding):**
- Involves a base class and a subclass.
- The subclass provides a specific implementation for a method that is already defined in its superclass.
- The decision on which method to invoke is made at runtime based on the actual type of the object.
```java
class Animal {
    void sound() {
        System.out.println("Animal makes a sound");
    }
}

class Dog extends Animal {
    void sound() {
        System.out.println("Dog barks");
    }
}
```
## References
1. https://stackify.com/oops-concepts-in-java/

2. https://www.youtube.com/watch?v=bSrm9RXwBaI
